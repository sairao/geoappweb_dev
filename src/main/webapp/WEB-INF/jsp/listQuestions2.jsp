<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="./InstructorPageHeader.jsp"%>
<%@ include file="./InstructorSideBar.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns:th="http://www.thymeleaf.org">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->

	<section class="content" style="margin-left: 20%">

		<section class="content-header">
			<span id="pinfound" style="font-size: 250%;"> </span>
		</section>
				
		<a href="/GeoApp/Pool/addQuestion"
			style="color: blue;">Add a new question</a><br><br>
		<table border="1">
			<tr>
				<td style="font-weight: bold;">Question No.</td>
				
				<td style="font-weight: bold;">Question Name</td>
				
				<td style="font-weight: bold;">Choice 1</td>
				
				<td style="font-weight: bold;">Choice 2</td>
				
				<td style="font-weight: bold;">Choice 3</td>
				
				<td style="font-weight: bold;">Choice 4</td>
				
				<td style="font-weight: bold;">Correct Option</td>
				
				<td style="font-weight: bold;">Edit</td>
				
				<td style="font-weight: bold;">Save</td>
				
				<td style="font-weight: bold;">Delete</td>
			</tr>
			<c:forEach items="${questionOptions}" var="questionOption">
				<tr>
					<td class="title" align="center"><span> 
					<input id="${questionOption.getQuestionNo()}" type="text" style="display:show;" readonly value="${questionOption.getquestionNo()}"/>
				
					</span></td>
										
					<td class="title" align="center"><span> 
					<input id="${questionOption.getQuestion()}" type="text" style="display:show;" readonly value="${questionOption.getQuestion()}"/>
					<input id="${questionOption.getQuestion()}h" type="text" value="${questionOption.getQuestion()}" style="display:none;" />
					</span></td>
					
					<c:forEach items="${questionOption.getChoices}" var="choice">
					
						<td class="title" align="center"><span> 
						<input id="${choice}" type="text" style="display:show;" readonly value="${choice}"/>
						<input id="${choice}h" type="text" value="${choice}" style="display:none;" />
						</span></td>
					
					</c:forEach>
					
					<td class="title" align="center"><span> 
					<input id="${questionOption.getCorrectChoice()}" type="text" style="display:show;" readonly value="${questionOption.getCorrectChoice()}"/>
					<input id="${questionOption.getCorrectChoice()}h" type="text" value="${questionOption.getCorrectChoice()}" style="display:none;" />
					</span></td>
					
					
					<td class="description" align="center"><a
						id="${questionOption.getPoolQuestionID()},,,${questionOption.getQuestion()},,,${questionOption.getChoices}" onclick="editPool(id)" style="cursor: pointer;" >Edit</a>
					</td>
					
					<td class="description" align="center"><a
						id="${pool.getPoolID()},,,${pool.getPoolName()},,,${pool.getPoolDesc()}" onclick="savePool(id)" style="cursor: pointer;" >Save</a>
					</td>
					
					<td class="description" align="center"><a
						href="/GeoApp/Pool/removePool/${pool.getPoolName()}">Delete</a>
					</td>
				</tr>
			</c:forEach>
		</table>
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<%@ include file="./Footer.jsp"%>


<script>

function editPool(id){
	var nameDesc = id.split(",,,");
	

	$("[id='"+ nameDesc[0] +"h']").show();
	$("[id='"+ nameDesc[1] +"h']").show();
	$("[id='"+ nameDesc[0] +"']").hide();
	$("[id='"+ nameDesc[1] +"']").hide();
	$("[id='"+ nameDesc[0] +"h']").val(nameDesc[0]);
	$("[id='"+ nameDesc[1] +"h']").val(nameDesc[1]);
	$("[id='"+ nameDesc[0] +"h']").focus();
	$("[id='"+ nameDesc[1] +"h']").focus();
	
	
}


function savePool(id){
	var nameDesc = id.split(",,,");
	
	$("[id='"+ nameDesc[1] +"']").show();
	$("[id='"+ nameDesc[2] +"']").show();
	
	$("[id='"+ nameDesc[1] +"h']").hide();
	$("[id='"+ nameDesc[2] +"h']").hide();

	$("[id='"+ nameDesc[1] +"']").val($("[id='"+ nameDesc[1] +"h']").val());
	$("[id='"+ nameDesc[2] +"']").val($("[id='"+ nameDesc[2] +"h']").val());
	
	var i1 = $("[id='"+ nameDesc[1] +"h']").val();
	var i2 = $("[id='"+ nameDesc[2] +"h']").val();
	
	$.ajax({
		type : "POST",
		url : "/GeoApp/Pool/updatePool/"+nameDesc[0]+"/"+i1+"/"+i2,
		dataType: "text",
		success : function(data) {
			alert(data);
			location.reload();
		}

	});
	event.preventDefault();
	
   	
}

</script>
