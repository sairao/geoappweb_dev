<%@page import="edu.nwmissouri.geoapp.model.TblUser"%> 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="./InstructorHomeHeader.jsp"%>

<%@ include file="./InstructorSideBar.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns:th="http://www.thymeleaf.org">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>



<div class="content-wrapper">
	<!-- Content Header (Page header) -->



	<section class="content">

		<section class="content-header">
			<span id="pinfound" style="font-size: 150%;"> </span>
		</section>
		
			<h3>List of Questions: </h3> <br>
		
				
		<a style="color: blue; cursor: pointer;" href="/GeoApp/Pool/addQuestion/${poolID}/${poolName}">Add a new question</a><br><br>
		<div>
		
		<input id="poolName" type="text" style=" border: 1px solid blue, display:show;" readonly value="${poolName}"/>
		<input id="poolNameh" type="text" style="display:none; "  value=""/>
		<input type="button" id="editPoolName" name="editPoolName" onclick = "editPoolName()" value="Edit Pool Name" />
		<p id="poolExistError" style="color: red"></p>
		
		<script>
		
		</script>
		
		<table border="1" id="questionOptionsTable">

			<thead>
			<tr>
				<td style="font-weight: bold; width:40px">No.</td>
				
				<td style="font-weight: bold;">Question Text</td>
				
				<td style="font-weight: bold;">Choice 1</td>
				
				<td style="font-weight: bold;">Choice 2</td>
				
				<td style="font-weight: bold;">Choice 3</td>
				
				<td style="font-weight: bold;">Choice 4</td>
				
				<td style="font-weight: bold;">Correct Option</td>
				
				<td style="font-weight: bold;">Edit</td>
										
				<td style="font-weight: bold;">Delete</td>
			</tr>
			</thead>
			
			<tbody>
			<c:forEach items="${questionOptions}" var="questionOption">
				<tr id="${questionOption.getQuestionNo()}question"  >
					<td ><span> 					
				
					${questionOption.getQuestionNo()}</span></td>
										
					<td><span> ${questionOption.getQuestion()}
					 
					</span></td>
					
					<c:forEach items="${questionOption.getChoices()}" var="choice">
					
						<td class="title" align="left" ><span> ${choice}
						</span></td>
					
					</c:forEach>
					
					<td  ><span> ${questionOption.getCorrectChoice()}					
					</span></td>
					
					
					
					<td ><a
						id="${questionOption.getQuestionNo()},,,${questionOption.getPoolQuestionID()}" onclick="editQuestionOptions(id)" style="cursor: pointer;" >Edit</a>
					</td>
														
					<td ><a
						id="${questionOption.getPoolQuestionID()}" onclick = "DeleteQuestionOptions(id)" style="cursor: pointer;">Delete</a>
					</td>
				</tr>
			</c:forEach>
			
			</tbody>
			
		</table>
		<br></br>
		</div>
		
		<div>                                                    
		         <input type="button" id="Save" name="Save" onclick = "saveQuestionOptions()" value="Save" />
		         
 					<input type="button" id="exportQuestions" onclick ="exportFile()" name="exportQuestions" value="Export Questions" />
		        	
		        	<input type="button" id="Cancel" name="Cancel" onclick ='location.href="/GeoApp/Pool/listPools"' value="Cancel" />
		        	
		        		<br><br>
		        		
		        		<form action="/GeoApp/Pool/uploadFiletoPool/${poolID}/${poolName}" method="post" enctype="multipart/form-data"  >
		        		<p> <b> Upload file to add questions to existing pool:  </b>
							<input type="file" name="documentcontent1" id="documentcontent1"  accept="text" required />
							<input type="submit" id="submitBtn" name="submitBtn"   value="Submit" /> 
						  </p>
		        		</form>     		        		
							
		        	</div>
					
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<%@ include file="./Footer.jsp"%>


<script>

$(document).ready(function () {
	
	$('input[type=file]').change(function () {
	var val = $(this).val().toLowerCase();
	var regex = new RegExp("(.*?)\.(txt)$");
	if(!(regex.test(val))) {
		$(this).val('');
		alert('Please select text file (.txt) only');
	} }); });

var rows = [];
var questionsEdited = [];
var poolQuestionIDs = [];			
var questions = [];
var options = [];
var correctChoices = [];
var oldPoolName = 	"";
var newPoolName = 	"";
var correctChoiceIds = [];
var editedPoolQuestionIds = "";



function editQuestionOptions(id){
	//string.indexOf(substring)  ::::: if not found will return -1
	
	if(editedPoolQuestionIds.indexOf(id.split(",,,")[1]) > -1){
		return false;
	}
	
	var row = id.split(",,,")[1];
	questionsEdited.push(id.split(",,,")[0]);
	
	editedPoolQuestionIds += id.split(",,,")[1] + " ";
	
	var temp = "";
	for(var i=2; i < 8; i++){
		temp += $('#questionOptionsTable tbody tr:nth-child('+id.split(",,,")[0]+') td:nth-child('+i+')').text().trim() + ",,,";
		$('#questionOptionsTable tbody tr:nth-child('+id.split(",,,")[0]+') td:nth-child('+i+')').html(' ');
	
	} 
	row += ",,," + temp;
	rows.push(row);
	
	/* var t = rows[0];
	console.log(t); */
	
	for(var i=2; i < 8; i++){
		if(i == 7){
			$('#questionOptionsTable tbody tr:nth-child('+id.split(",,,")[0]+') td:nth-child('+i+')').append($('<input type="text" id="'+row.split(",,,")[i-1].trim()+'" value="'+row.split(",,,")[i-1].trim()+'" />'));
			correctChoiceIds.push(row.split(",,,")[i-1].trim());			
			
		}else{
			$('#questionOptionsTable tbody tr:nth-child('+id.split(",,,")[0]+') td:nth-child('+i+')').append($('<input type="text" id="'+row.split(",,,")[i-1].trim()+'" value="'+row.split(",,,")[i-1].trim()+'" />'));	
		}
				
	}
	
	
	event.preventDefault();
	
	// For reference
	//var nameDesc = $('#questionOptionsTable tbody tr:nth-child('+id.split(",,,")[0]+') td:nth-child(2)').text();
     //var texto = $('#questionOptionsTable tbody tr:nth-child(1) td:nth-child(2)').text();
    //alert(nameDesc); 
     
    
    /* $('#questionOptionsTable tr td').each(function() {
        var text = $(this).text();
        alert(text);
    });  */
    
    

}



function editPoolName() {
	
	$('#poolName').hide();
	oldPoolName = $('#poolName').val();
	$('#poolNameh').show();
	$('#poolNameh').val(oldPoolName);
	$('#poolNameh').focus();
	//alert($('#poolNameh').val());
}



function saveQuestionOptions(){
	
	$('#poolExistError').html("");
	
	 var val = 0;
	 
	 for(var i=0; i<correctChoiceIds.length; i++){
			
			val = $("[id='"+ correctChoiceIds[i] +"']").val();
			if(val<= 0 || val>4){
		
				$("[id='"+ correctChoiceIds[i] +"']").css({"background-color":"#ff8080"}).focus();			
				alert("Please select choices from 1-4 integer values only");
				return false;
			} 
		}
	
	
	
	
	if(oldPoolName == ""){
		saveQuesOpt();
		return false;
	}
	newPoolName = 	$('#poolNameh').val();
	
	if(newPoolName == ""){
		alert("Pool Name can't be empty");
		return false;
	}
	
	if(oldPoolName.localeCompare(newPoolName) == 0){
		
		/* if(questionsEdited.length == 0) {
		alert("No changes have been made!");
		
		} */
		
		$('#poolNameh').hide();
		$('#poolName').show();
		$('#poolName').val(newPoolName);
		oldPoolName = 	"";
		newPoolName = 	"";
		saveQuesOpt();

		
	}else{
		$.ajax({
			type : "GET",
			url : "/GeoApp/Pool/checkPoolNameAndSave/${poolID}/" + newPoolName ,
			data : " "	

		}).then(function(data, status, jqxhr){
			poolNameCheck(data);
			/* alert("Changes to the pool name has been saved!"); */
		});
	}
	
}	

	function poolNameCheck(data1, e){
		if(data1 == false){
			rows = [];
			questionsEdited = [];
			poolQuestionIDs = [];			
			questions = [];
			options = [];
			correctChoiceIds = [];
			correctChoices = [];
			editedPoolQuestionIds = "";
			newPoolName = 	"";
			$('#poolExistError').html("Pool Name already exists");	
			e.preventDefault();
		}else{
			saveQuesOpt();
/* 			alert("Changes to the pool name has been saved!");
 */	}
	
   	
	}
		
		
		function saveQuesOpt(){
			
			
			
			if(oldPoolName != ""){
				$('#poolNameh').hide();
				$('#poolName').show();
				$('#poolName').val(newPoolName);	
				oldPoolName = 	"";
				newPoolName = 	"";
			}
			
			
			
			if((oldPoolName == "" && questionsEdited.length > 0) || (oldPoolName != "" && questionsEdited.length > 0)){
				
							
				
				for(var i=0; i<rows.length; i++){
					
					var nameDesc = rows[i].split(",,,");
					
					var poolQuestionID = nameDesc[0];
					var question = $("[id='"+ nameDesc[1] +"']").val();
					
					for(var j=2; j<6; j++){
						//alert($("[id='"+ nameDesc[j] +"']").val());
						options.push($("[id='"+ nameDesc[j] +"']").val());
					}
					
					var correctChoice = $("[id='"+ nameDesc[6] +"']").val();
					
					//alert(options[4*i]+ " " + options[(4*i)+1]+ " "+options[(4*i)+2]+' '+options[(4*i)+3]);
					
					poolQuestionIDs.push(poolQuestionID);
					questions.push(question);
				
					correctChoices.push(correctChoice);
					
					$('#'+questionsEdited[i]+'question').remove();
					
					var newRow = '<tr><td>'+questionsEdited[i]+'</td><td>'+question+'</td><td>'+options[4*i]+'</td><td>'+options[4*i+1]+'</td><td>'+options[4*i+2]+'</td><td>'+options[4*i+3]+'</td><td>'+correctChoice+'</td><td>'+  '<a id="'+questionsEdited[i]+ ",,," +poolQuestionID+'" onclick="editQuestionOptions(id)" style="cursor: pointer;" >Edit</a>'   +   '</td><td>'+     '<a id="'+poolQuestionID+'" onclick="DeleteQuestionOptions(id)" style="cursor: pointer;" >Delete</a>'    +'</td></tr>'; 
					 
				    //$('#questionOptionsTable > tbody > tr').eq(0).before(newRow1);
				    if(questionsEdited[i]-2 == -1){
				    	$('#questionOptionsTable > tbody > tr').eq(0).before(newRow);
				    }else{
				    	$('#questionOptionsTable > tbody > tr').eq(questionsEdited[i]-2).after(newRow);	
				    }
				    
				}
				
				
						
				var json = {
						"poolQuestionIDs" : poolQuestionIDs,
						"questions" : questions,
						"options" : options,
						"correctChoices" : correctChoices						
				};
				
				
				$.ajax({
			        url : "/GeoApp/Pool/updateQuestionOptions/${poolID}",
			        data : JSON.stringify(json),
			        type : "POST",
			        contentType: 'application/json',
			        dataType: "text",
			        beforeSend : function(xhr) {
			               xhr.setRequestHeader("Accept", "application/json");
			               xhr.setRequestHeader("Content-Type", "application/json");
			        },
			        success : function(data) {	
			        	
						location.reload();
						alert("Changes have been saved!");
			        }
			  });
				
				event.preventDefault();
				rows = [];
				questionsEdited = [];
				poolQuestionIDs = [];			
				questions = [];
				options = [];
				correctChoiceIds = [];
				correctChoices = [];
				editedPoolQuestionIds = "";
				newPoolName = 	"";
			}
			
			
			
			if(oldPoolName == ""){
				return false;
			}
			
			
			
			
		}
			
		
		
	
	
		
   	




function DeleteQuestionOptions(id){
	 
	  $.ajax({
	        url : "/GeoApp/Pool/removeQuestions/" + id ,
	        data : "",
	        type : "GET",
	         dataType: "text",
	        
	        success : function(data) {	
	        	alert(data);
				location.reload();;
	        }
	  });
	  event.preventDefault();
}


function AddQuestionOptions(){
	
	$('#questionOptionsTable tbody').append($('<tr><td>value</td></tr>'));
	
	
}



function exportFile(){
	$.ajax({
        url : "/GeoApp/Pool/exportFile/${poolID}/${poolName}" ,
        data : "",
        type : "GET",
        dataType: "text",        
        success : function(data) {	
        	alert("Pool has been successfully exported to the path c:/"+data+".txt");			
        }
  });
  event.preventDefault();
	
	
}


</script>

<style>

table {
    display: block;
    overflow-x: auto;
}

#poolNameh:focus { background-color:  #99ff99;}


</style>


