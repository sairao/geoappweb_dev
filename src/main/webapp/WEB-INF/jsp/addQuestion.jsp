<%@page import="edu.nwmissouri.geoapp.model.TblUser"%> 
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="./InstructorHomeHeader.jsp"%>
<%@ include file="./InstructorSideBar.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns:th="http://www.thymeleaf.org">
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script type="text/javascript"
	src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	
	
<script type="text/javascript">

</script>

<style>

.txtbox {
 border: none;
 width: 100%;
}
input {
	font-size: 17px;
    height: 30px;	
}
table {
	 background: none repeat scroll 0 0 #abcdef;
    border: 1px solid #000;	
}
</style>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->

	<section class="content">

		<section class="content-header">
			<span id="pinfound" style="font-size: 250%;"> </span>
		</section>
		
			<h3>Add a new question : </h3> <br>
		
		
		<div>
      <input type="button" value="Add a row" class="plusbtn" />
      <input type="button" value="Remove" class="minusbtn" />
</div> <br>
    
<table id="AddNewQuestionOptions" border="1" cellpadding="1" cellspacing="1" class="test">
  <tr>
    <td>Question Text</td>
    <td>Choice 1</td>
    <td>Choice 2</td>
    <td>Choice 3</td>
    <td>Choice 4</td>
    <td>Correct Option</td>
  </tr>
  <tr>
    <td><input id="100" type="text" class="txtbox" value="" /></td>
    <td><input id="101" type="text" class="txtbox" value="" /></td>
    <td><input id="102" type="text" class="txtbox" value="" /></td>
    <td><input id="103" type="text" class="txtbox" value="" /></td>
    <td><input id="104" type="text" class="txtbox" value="" /></td>
    <td><input id="105" type="text" class="txtbox choiceCheck" value="" placeholder="numbers 1-4" /></td>
    
  </tr>
</table>
	<br>
	<p>                                                    
		<input type="button" id="Save" name="Save" onclick = "saveNewQuestionOptions()" value="Save" />
		
		<input type="button" id="Cancel" name="Cancel" onclick ='location.href="/GeoApp/Pool/showQuestions/${poolID}/${pname}"' value="Cancel" />
	</p>

			
	</section>
	<!-- /.content -->
</div>

<script>
	
var Textid = 106;
$('.plusbtn').click(function() {

	$(".test").append('<tr><td><input id="'+Textid+'" type="text" class="txtbox" value="" /></td><td><input id="'+ (Textid+1) +'" type="text" class="txtbox" value="" /></td><td><input id="'+ (Textid+2) +'" type="text" class="txtbox" value="" /></td><td><input id="'+ (Textid+3) +'" type="text" class="txtbox" value="" /></td><td><input id="'+ (Textid+4) +'" type="text" class="txtbox" value="" /></td><td><input id="'+ (Textid+5) +'" placeholder = "numbers 1-4" type="text" class="txtbox choiceCheck" value="" /></td></tr>');
	Textid += 6;
});

$('.minusbtn').click(function() {
	if($(".test tr").length != 2)
		{
			$(".test tr:last-child").remove();
			Textid -= 6;
		}
   else
		{
			alert("You cannot delete first row");
		}
});

var questions = [];
var options = [];
var correctChoices = [];




$(".choiceCheck").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && ((e.which < 48) || (e.which > 57))) {
       //display error message
       //$("#errmsg").html("Digits Only").show().fadeOut("slow");
       alert("Please enter numbers for choices ");
       return false;
   }
  });



function saveNewQuestionOptions(){
	
	
	var rowCount = $('#AddNewQuestionOptions tr').length;
	var currentId = 100;
	
	
	
	
	if(($("[id='"+ 100 +"']").val()) != "" && $("[id='"+ 101 +"']").val() != "" && $("[id='"+ 102 +"']").val() != "" && $("[id='"+ 103 +"']").val() != "" && $("[id='"+ 104 +"']").val() != "" && $("[id='"+ 105 +"']").val() != ""){
		
		
		for(i=106; i<Textid; i++){
			if(($("[id='"+ (i) +"']").val()) == "" || $("[id='"+ (i+1) +"']").val() == "" || $("[id='"+ (i+2) +"']").val() == "" || $("[id='"+ (i+3) +"']").val() == "" || $("[id='"+ (i+4) +"']").val() == "" || $("[id='"+ (i+5) +"']").val() == ""){
				alert("Please enter all the fields");
				return false;
			}
		}

		
		
		
		for(var i=0; i < rowCount-1 ; i++){
			
	    	for(var j=0; j < 6 ; j++){			//105
	    		
	    		if(j == 0){
	    			questions.push($("[id='"+ currentId +"']").val());
	    			currentId++;
	    		}else if(j==1 || j==2 || j==3 || j==4){
	    			options.push($("[id='"+ currentId +"']").val());
	    			currentId++;
	    		}else{
	    			if( $("[id='"+ currentId +"']").val() < 1  ||    $("[id='"+ currentId +"']").val() > 4 ){
	    				questions = [];
	    				options = [];
	    				correctChoices = [];
	    				alert("Allowable range for choices is 1-4");
	    				$("[id='"+ currentId +"']").css({"background-color":"#ff8080"}).focus();			

	    				return false;
	    			}else{
	    				correctChoices.push($("[id='"+ currentId +"']").val());
		    			currentId++;	
	    			}
	    			
	    		}
	    			
	    	}
			
		}


	    var json = {
				"questions" : questions,
				"options" : options,
				"correctChoices" : correctChoices	
		};
		
	    var poolID = '${poolID}';
	     
		$.ajax({
	        url : "/GeoApp/Pool/savenewQuestionOptions/"+poolID,
	        data : JSON.stringify(json),
	        type : "POST",
	        contentType: 'application/json',
	        dataType: "text",
	        beforeSend : function(xhr) {
	               xhr.setRequestHeader("Accept", "application/json");
	               xhr.setRequestHeader("Content-Type", "application/json");
	        },
	        success : function(data) {	
	        	
				window.location.href="/GeoApp/Pool/showQuestions/${poolID}/${pname}";
	        }
	  });
		
		event.preventDefault();
				
		questions = [];
		options = [];
		correctChoices = [];

	}else{
		alert("Please enter all the fields");
	}
    

}

</script>



<!-- /.content-wrapper -->



<%@ include file="./Footer.jsp"%>


