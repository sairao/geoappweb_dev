<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="./InstructorPageHeader.jsp"%>
<%@ include file="./InstructorSideBar.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns:th="http://www.thymeleaf.org">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->

	<section class="content" style="margin-left: 20%">

		<section class="content-header">
			<span id="pinfound" style="font-size: 250%;"> </span>
		</section>
		<h3> Quiz Grades of all students: </h3>		
		
		<table id="gradeTable" border="1">
			<tr>
				<td style="font-weight: bold;">Student Name</td>
<!-- 				<td style="font-weight: bold;">Quiz ID</td>
 -->				
<!-- 				<td style="font-weight: bold;">Student Name</td>
 -->				
				<td style="font-weight: bold;">Quiz Name</td>
				
				<td style="font-weight: bold;">Max Score</td>
				
				<td style="font-weight: bold;">Attempts</td>
				
				<td style="font-weight: bold;">Status</td>
				
				<td style="font-weight: bold;">Give Access</td>
										
			</tr>
			<c:forEach items="${studentQuizzes}" var="studentQuiz" varStatus="theCount" >
				<tr>
					<td id = "${studentQuiz.getTblStudent().getStudentID()}" class="studentName" align="center"><span> <c:out
								value="${studentNames[theCount.index]}" />
					</span></td>
					<%-- <td class="quizID" align="center"><span> <c:out
								value="${studentQuiz.getTblQuiz().getQuizID()}" /></span></td> --%>
					
					<%-- <td class="studentName" align="center"><span> <c:out
								value="${studentQuiz.getTblQuiz().getQuizName()}" /></span></td> --%>
					<td id="${studentQuiz.getTblQuiz().getQuizID()}" class="quizName" align="center"><span> <c:out
								value="${studentQuiz.getTblQuiz().getQuizName()}" /></span></td>
					<td class="MaxScore" align="center"><span> <c:out
								value="${studentQuiz.getMaxScore()}" /></span></td>
					<td class="NumberofAttempts" align="center"><span> <c:out
								value="${studentQuiz.getNumTakes()}" /></span></td>
					<td class="Grade" align="center"><span> <c:out
								value="${studentQuiz.getGrade()}" /></span></td>
					<td class="Access" align="center"><span>  
					<input  type="checkbox" title = "Checking this box would change the quiz status to 'pass' irrespective of the student score" value="setAccess" name="setAccess" />
			
			</span></td>
					
								
					
				</tr>
			</c:forEach>
		</table>
		<br>
		
		<p>
			<input  type="button" title= "This will allow the students to bypass the Quiz irrrespective of their quiz status" 
			value="Give Access to Assignments" name="Give Access to Assignments" onclick="setAccess()">
			<input type="button" id="Cancel" name="Cancel" onclick ='location.href="/view/instructor"' value="Cancel" />
			
			</p>
		
		
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<%@ include file="./Footer.jsp"%>


<script>

var studentIDs = [];
var quizIDs = [];

function setAccess() {
	
	var rowCount = $('#gradeTable tr').length;
    
	for(var i=2; i<=rowCount; i++){
		if($('#gradeTable  tr:nth-child('+i+') td:nth-child(6)').find('input[type="checkbox"]').is(':checked')){
			
			
			studentIDs.push($('#gradeTable  tr:nth-child('+i+') td:nth-child(1)').attr('id'));
			quizIDs.push($('#gradeTable  tr:nth-child('+i+') td:nth-child(2)').attr('id'));
						
			//studentIDs.push($('#gradeTable  tr:nth-child('+i+') td:nth-child(1)').text());
			//quizIDs.push($('#gradeTable  tr:nth-child('+i+') td:nth-child(2)').text());
		}
	}
	
	var json = {
			'studentIDs' : studentIDs,
			'quizIDs' : quizIDs
	};
	
	$.ajax({
        url : "/GeoApp/Pool/setAccess",
        data : JSON.stringify(json),
        type : "POST",
        contentType: 'application/json',
        dataType: "text",
        beforeSend : function(xhr) {
               xhr.setRequestHeader("Accept", "application/json");
               xhr.setRequestHeader("Content-Type", "application/json");
        },
        success : function(data) {	
        	alert(data);
			location.reload();
        }
  });
  event.preventDefault(); 
	
	
	
}


</script>
