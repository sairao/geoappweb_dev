<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ include file="./InstructorSideBar.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="./InstructorHomeHeader.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>\



<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script type="text/javascript"
	src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<div class="content-wrapper"> 
	<!-- Content Header (Page header) -->
	<br> <br>
	<h3 style="padding: 15px">Create Quiz Pool</h3>
	

	<section class="content">

		<section class="content-header">
			
		</section>
		
		<!-- onsubmit ="return checkform() " -->
		<form action="/GeoApp/Pool/uploadandreadtextdoc" method="post" enctype="multipart/form-data"  >     

			 	
					
					<table>
						<tr>
							<td>Pool Name:</td>					
							<td><input type="text" id = "poolName" name="PoolName" placeholder = "Ex: Rock pool" required></td>
							<td><p id="poolExistError" style="color: red"></p></td>
						</tr>
						
						 <!-- pool description should be unique -->
						 
						<!-- <tr>
							<td>Pool Description:</td>					
							<td><input type="text" name="PoolDesc" placeholder = "Ex: Rock pool"></td>
						</tr> -->
						
						

						<tr>
					
							<td>Upload quiz:</td>  
							<td><input type="file" name="documentcontent1" id="documentcontent1"  accept="text" required /> </td>
						</tr>
					
					</table> <br>
							<p>                                                    
		        			<input type="submit" id="submitBtn" name="submitBtn"   value="Submit" />
		        			<input type="button" id="Cancel" name="Cancel" onclick ='location.href="/GeoApp/Pool/listPools"' value="Cancel" />
		        			</p>
		        	
		</form>


	</section>
	<!-- /.content -->
</div>

<script> 

$(document).ready(function () {
	
	$('input[type=file]').change(function () {
	var val = $(this).val().toLowerCase();
	var regex = new RegExp("(.*?)\.(txt)$");
	if(!(regex.test(val))) {
		$(this).val('');
		alert('Please select text file (.txt) only');
	} }); });


$('form').submit(function(e){
	event.preventDefault();
	var poolName = $('#poolName').val();
	var t = 0;
	$('#poolExistError').html("");	
	
	$.ajax({
		type : "GET",
		url : "/GeoApp/Pool/checkPoolName/" + poolName ,
		data : "",
		dataType: "text" 

	}).then(function(data, status, jqxhr){

		if(data == "false"){
			
			$('#poolExistError').html("Pool Name already exists");					
			return false;			
	 	}else{	
	 		$('form').unbind('submit').submit();
			 return true;
	 	}	
	});
	

	
	
});


/* function  checkform() {
	event.preventDefault();
	
	var poolName = $('#poolName').val();
	var t = 0;
	$('#poolExistError').html("");	
	$.ajax({
		type : "GET",
		url : "/GeoApp/Pool/checkPoolName/" + poolName ,
		data : "",	
		dataType: "text" 

	}).then(function(data, status, jqxhr){
		
		 
		myfunction(data);
		
		
	});
	//event.preventDefault();
} */

</script>
<!-- /.content-wrapper -->
<%@ include file="./Footer.jsp"%>