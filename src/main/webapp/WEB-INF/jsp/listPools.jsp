<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="./InstructorHomeHeader.jsp"%>
<%@ include file="./InstructorSideBar.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns:th="http://www.thymeleaf.org">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>



<div class="content-wrapper">

	<!-- Content Header (Page header) -->
	
	

	<section class="content" > <!-- style="margin-left: 20%" -->
        
		<section class="content-header">
			<span id="pinfound" style="font-size: 250%;">
			 </span>
		</section>
		
	<h3>List of Pools: </h3> <br>
				
		<a href="/GeoApp/Pool/redirecttoupload"
			style="color: blue;">Create Quiz Pool</a><br><br>
		<table border="1">
			<tr>
				<!-- <td style="font-weight: bold;">Pool No.</td> -->
				
				<td style="font-weight: bold;">Pool Name</td>
				
				<td style="font-weight: bold;">Edit Pool Questions</td>
				
				<!-- <td style="font-weight: bold;">Change Pool Name</td> -->
				
				<!-- <td style="font-weight: bold;">Save</td> -->
				
				<td style="font-weight: bold;">Delete</td>
				
			</tr>
			<c:forEach items="${pools}" var="pool">
				<tr>
					<td class="title" ><span> <%-- <c:out
								value="${pool.getPoolName()}" /> --%>
								
					<input id="${pool.getPoolName()}" type="text" style=" border:  1px solid blue, display:show;" readonly value="${pool.getPoolName()}"/>
					<input id="${pool.getPoolName()}h" type="text" value="" style="display:none;" />
					</span> </td>
					
					
					<!-- <td><input id="title" type="text" style="display:show;" readonly value="Paper Title"/></td>							
					<td><input id="saveTitle" type="text" value="" style="display:none;" /></td> -->
					
					<%-- <td class="title" align="center"><span> <c:out
								value="${pool.getPoolDesc()}" />
					<input id="${pool.getPoolDesc()}" type="text" style="display:show;" readonly value="${pool.getPoolDesc()}"/>
					<input id="${pool.getPoolDesc()}h" type="text" value="" style="display:none;" />
					</span></td> --%>
					
					<%-- <td class="description" align="center"><a
						onClick="editPool(${pool.getPoolName()}, ${pool.getPoolDesc()})" >Edit</a></td> --%>
						
					<td class="description" align="center"><a
						href="/GeoApp/Pool/showQuestions/${pool.getPoolID()}/${pool.getPoolName()}">Edit Pool Questions</a>	
					
					<%-- <td class="description" align="center"><a
						id="${pool.getPoolID()},,,${pool.getPoolName()}" onclick="editPool(id)" style="cursor: pointer;" >Change Pool Name</a>
					</td> --%>
					
					<%-- <td class="description" align="center"><a
						id="${pool.getPoolID()},,,${pool.getPoolName()},,,${pool.getPoolDesc()}" onclick="savePool()" style="cursor: pointer;" >Save</a>
					</td> --%>
					
					<td class="description" align="center"><a
						 id="${pool.getPoolID()}" onclick = "confirmSubmission(id)" style="cursor: pointer;" >Delete</a>
					</td>
				</tr>
			</c:forEach>
		</table> <br>
						<div>
<!-- 			                <input type="button" id="Save" name="Save" onclick = "savePool()" value="Save" />
 -->		        			<input type="button" id="Cancel" name="Cancel" onclick ='location.href="/GeoApp/view/instructor"' value="Cancel" />
		        
		        			</div>
		        			
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<%@ include file="./Footer.jsp"%>


<script>

var namesDescs = [];

function editPool(id){
	var nameDesc = id.split(",,,");
	namesDescs.push(id);

	$("[id='"+ nameDesc[1] +"h']").show();
	$("[id='"+ nameDesc[2] +"h']").show();
	$("[id='"+ nameDesc[1] +"']").hide();
	$("[id='"+ nameDesc[2] +"']").hide();
	$("[id='"+ nameDesc[1] +"h']").val(nameDesc[1]);
	$("[id='"+ nameDesc[2] +"h']").val(nameDesc[2]);
	$("[id='"+ nameDesc[1] +"h']").focus();
	$("[id='"+ nameDesc[2] +"h']").focus();
	
	
}


function savePool(){
	
	if(namesDescs.length == 0){
		return false;
	}
	
	var ids = [];
	var names = [];
	var descs = [];
	
	for(var i=0; i<namesDescs.length; i++){
		var nameDesc = namesDescs[i].split(",,,");
		
		$("[id='"+ nameDesc[1] +"']").show();
		$("[id='"+ nameDesc[2] +"']").show();
		
		$("[id='"+ nameDesc[1] +"h']").hide();
		$("[id='"+ nameDesc[2] +"h']").hide();

		$("[id='"+ nameDesc[1] +"']").val($("[id='"+ nameDesc[1] +"h']").val());
		$("[id='"+ nameDesc[2] +"']").val($("[id='"+ nameDesc[2] +"h']").val());
		
		var i1 = $("[id='"+ nameDesc[1] +"h']").val();
		var i2 = $("[id='"+ nameDesc[2] +"h']").val();
		
		ids.push(nameDesc[0]);
		names.push($("[id='"+ nameDesc[1] +"h']").val());
		descs.push($("[id='"+ nameDesc[2] +"h']").val());
	}
	
	var json = {
			'ids' : ids,
			'names' : names,
			'descs' : descs
	}; 
	
	
	$.ajax({
        url : "/GeoApp/Pool/updatePool",
        data : JSON.stringify(json),
        type : "POST",
        contentType: 'application/json',
        dataType: "text",
        beforeSend : function(xhr) {
               xhr.setRequestHeader("Accept", "application/json");
               xhr.setRequestHeader("Content-Type", "application/json");
        },
        success : function(data) {	
        	alert(data);
			location.reload();
        }
  });
  event.preventDefault(); 	
  
  
}

function confirmSubmission(id) {
	//console.log(data1.options);

	if (confirm("Are you sure you want to delete the selected pool?")) {
		deletePool(id);

	}
}
function deletePool(id){
	  
	  
	  $.ajax({
	        url : "/GeoApp/Pool/removePool/" + id ,
	        data : "",
	        type : "GET",
	         dataType: "text",
	        
	        success : function(data) {	
	        	alert(data);
				location.reload();
				
	        }
	  });
	  event.preventDefault();

		
}



</script>
