package edu.nwmissouri.geoapp.controller;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.nwmissouri.geoapp.model.TblAssignment;
import edu.nwmissouri.geoapp.model.TblPool;
import edu.nwmissouri.geoapp.model.TblPoolquestion;
import edu.nwmissouri.geoapp.model.TblPoolquestionoption;
import edu.nwmissouri.geoapp.model.TblQuiz;
import edu.nwmissouri.geoapp.model.TblStudentquiz;
import edu.nwmissouri.geoapp.model.TblUser;
import edu.nwmissouri.geoapp.repository.PoolRepository;
import edu.nwmissouri.geoapp.serviceImpl.AsssignmentServiceImpl;
import edu.nwmissouri.geoapp.serviceImpl.PoolServiceImpl;
import edu.nwmissouri.geoapp.serviceImpl.UserServiceImpl;


@Controller
@RestController
@RequestMapping("/GeoApp/Pool")
public class PoolController {
	
	@Autowired
	private UserServiceImpl service;
	
	@Autowired
	private PoolServiceImpl poolserviceimpl;
	
	@Autowired
	private UserServiceImpl userServiceImpl;
	
	@Autowired
	private AsssignmentServiceImpl asssignmentServiceImpl;
	
	@Autowired
	private PoolRepository poolRepository;
	
	
		
	//  It will show the Quiz page for the student 
	@RequestMapping(method = RequestMethod.GET, value = "/home")
	
	public ModelAndView showPoolHome(@RequestParam("assignID") Integer assignID,
			@RequestParam("studentID") Integer studentId) {
	
		Map<String,Object> model=new HashMap<>();		
		
		 model.put("studentID", studentId);
		 model.put("assignID", assignID);
		 
		return new ModelAndView("QuizPage", "quizmodel", model);
	}
	
//  It will show the Quiz page for the student 
	@RequestMapping(method = RequestMethod.GET, value = "/home2")
	
	public ModelAndView showPoolHome2(@RequestParam("assignID") Integer assignID,
			@RequestParam("studentID") Integer studentId) {
	
		Map<String,Object> model=new HashMap<>();		
		
		 model.put("studentID", studentId);
		 model.put("assignID", assignID);
		 
		return new ModelAndView("quizPage.html", "quizmodel", model);
	}
	
	// Checks attempts of quizzes taken 
	@RequestMapping(method = RequestMethod.GET, value = "/checkattempts/{studentID}/{assignID}")
	public @ResponseBody String checkAttempts(@PathVariable Integer studentID, @PathVariable Integer assignID) throws ParseException {
	
	
	return poolserviceimpl.checkAttempts(assignID,studentID );
	
	}


   // Checks the attempts before taking Quiz
	@RequestMapping(method = RequestMethod.GET, value = "/checkattemptsBeforeTakeQuiz/{studentID}/{assignID}")
	public @ResponseBody String checkAttemptsBeforeTakeQuiz(@PathVariable Integer studentID, @PathVariable Integer assignID) throws ParseException {
	
	return poolserviceimpl.checkAttemptsBeforeTakeQuiz(assignID,studentID );
	
	}

   //  Saving Quiz Score to the database 
	@RequestMapping(method = RequestMethod.POST, value = "/saveScore/{score}/{studentID}/{assignID}")	
	public void saveScore(@PathVariable Integer score, @PathVariable Integer studentID, @PathVariable Integer assignID) throws ParseException {	
		
	poolserviceimpl.saveScore(score, studentID, assignID);
	
	}

	// Displayig the score and result to the Student 
	@RequestMapping(value = "/quizResults", method = RequestMethod.GET)
	public ModelAndView showResultsPage(@RequestParam("questionsInQuiz") Integer questionsInQuiz,
			@RequestParam("scoreInQuiz") Integer scoreInQuiz, @RequestParam("assignID") Integer assignID,
			@RequestParam("studentID") Integer studentId) 
					{
		 
		TblAssignment assignment = service.getAssignment(assignID); 
		TblQuiz tblQuiz = assignment.getTblQuiz();

		 // To find percentage of marks got by a user
		 float q = questionsInQuiz;
		 float s = scoreInQuiz;
		 int minpercent = tblQuiz.getQualpercent(); 
		 int markspercent = (int) ((s/q) *100); 
		 int noofgivenattempts = tblQuiz.getNum_Takes_Max();  		 
		 // finding out whether the user has failed or passed the test
		 String passorfail = "";
		 if ( markspercent >= minpercent) {
			 passorfail = "Congratulations! You have passed the Quiz!";
			 
		 }else 
		 {
			 passorfail = "Sorry, you have failed the Quiz. Better Luck Next Time !";
		 }
				
		  TblUser tbluser = userServiceImpl.findbyuserID(studentId); 
		  
		  
		 
		 Map<String,Object> model=new HashMap<>();	
		 
		 model.put("studentID", studentId);
		 model.put("assignID", assignID);		
		 model.put("questionsInQuiz", questionsInQuiz);
		 model.put("scoreInQuiz", scoreInQuiz);
		 model.put("minpercent", minpercent);
		 model.put("markspercent", markspercent);
		 model.put("passorfail", passorfail);
		 model.put("noofgivenattempts", noofgivenattempts);
		 model.put("User", tbluser);
		 
		 return new ModelAndView("QuizResultsPage","quizresultsmodel",model);
		
		
	}
		
    // populating quiz questions from the database 
	@RequestMapping(method = RequestMethod.GET, value = "/getquestions/{assignID}/{studentID}")
	public @ResponseBody QuestionChoicesWrapper getPoolQuestions(@PathVariable Integer assignID,  @PathVariable Integer studentID) {

				
		int quizID =  asssignmentServiceImpl.findAssignmentById(assignID).getTblQuiz().getQuizID();
		//3000026
		int poolID; 
		
		TblPool tblPool = poolserviceimpl.findTblPoolByassignID(assignID);	//97 -- kris pool
		
		
		String poolName = tblPool.getPoolName();    // kris pool
/*		String poolDesc = tblPool.getPoolDesc();
*/		
		TblPool tblPool2 = poolserviceimpl.findPoolIDBypoolName(poolName); //95
		
		poolID = tblPool2.getPoolID();
		
		/*if(tblPool.getPoolID() == Integer.parseInt(tblPool.getPoolName().split(" ")[1])){
			poolID = tblPool.getPoolID();
		}else{
			poolID = Integer.parseInt(tblPool.getPoolName().split(" ")[1]);
		}
		*/
		TblQuiz tblQuiz = poolserviceimpl.findTblQuizbyQuizID(quizID);
		
		
		String timer = tblQuiz.getTimer();
		
		List<TblPoolquestion> qlist1 = new ArrayList<TblPoolquestion>();
		List<String> qlist2 = new ArrayList<String>();
		List<TblPoolquestionoption> choicelist1 = new ArrayList<TblPoolquestionoption>();
		List<String> choicelist2 = new ArrayList<String>();
		List<Integer> options = new ArrayList<Integer>();

		// numQuestions is the number of Questions that should randomly come
		// when a student takes a quiz , given a QuizID
		

		int numQuestionsperQuiz = Integer.parseInt(poolserviceimpl.getCountofQuestions(quizID));

		try {
            
			qlist1 = poolserviceimpl.getQuestions(poolID);		// 83  84  85
/*			choicelist1 = poolserviceimpl.getQuestionChoices();
*/
			int k = 0;

			// noofquestions is the total number of questions given a poolID

			int noofquestions = 0;

			noofquestions = qlist1.size();		// 3

			// method to randomize the no. of pool questions
			ArrayList<Integer> list = new ArrayList<Integer>();

			for (int i = 0; i < noofquestions; i++) {
				list.add(new Integer(i));
			}
			
			Collections.shuffle(list);		//  3  2   1

			// Random rg = new Random();

			for (int i = 0; i < numQuestionsperQuiz; i++) { // To generate and
															// send 3 (i<3)
															// Random questions

				// k= rg.nextInt(15);
				k = list.get(i);

			qlist2.add(qlist1.get(k).getQuestion()); // add total no. of question in place
				
											// of 5
			List<TblPoolquestionoption> choicesList =	poolserviceimpl.getChoices(qlist1.get(k).getPoolQuestionID());
				
				for (TblPoolquestionoption choices : choicesList )
				{
					choicelist2.add(choices.getChoice());
					options.add(choices.getFractionCorrect());
					
				}

				/*for (int j = (4 * k); j < 4 * k + 4; j++) {		// 12   13   14   15

					choicelist2.add(choicelist1.get(j).getChoice());
					options.add(choicelist1.get(j).getFractionCorrect());
				}*/

			}

		} catch (Exception e) {

		}

		QuestionChoicesWrapper qcw = new QuestionChoicesWrapper(qlist2, choicelist2, options, timer);

		return qcw;
	}
// wrapper class for questionlist, choicelist, options, timer 
	public class QuestionChoicesWrapper {

		List<String> questionlist;
		List<String> choicelist;
		List<Integer> options;
		String timer; 

		public QuestionChoicesWrapper() {
			super();
		}

		public QuestionChoicesWrapper(List<String> questionlist, List<String> choicelist, List<Integer> options, String timer) {
			super();
			this.questionlist = questionlist;
			this.choicelist = choicelist;
			this.options = options;
			this.timer = timer;
		}

		public List<String> getQuestionlist() {
			return questionlist;
		}

		public void setQuestionlist(List<String> questionlist) {
			this.questionlist = questionlist;
		}

		public List<String> getChoicelist() {
			return choicelist;
		}

		public void setChoicelist(List<String> choicelist) {
			this.choicelist = choicelist;
		}

		public List<Integer> getOptions() {
			return options;
		}

		public void setOptions(List<Integer> options) {
			this.options = options;
		}

		public String getTimer() {
			return timer;
		}

		public void setTimer(String timer) {
			this.timer = timer;
		}

	}
	
	
	    // uploading a document and parsing the questions and options and answers and saving them to different tables to the database
	
		@RequestMapping(method = RequestMethod.GET, value = "/redirecttoupload")	
		public ModelAndView Redirectoread() {	
		
		return new ModelAndView("CreateQuizPool");
		
	}
		// uploading the text document to create a quiz pool 
		
		@RequestMapping("/uploadandreadtextdoc")	
		public ModelAndView fileread(HttpServletRequest  request1) throws IOException {
			
			if(request1.getSession().getAttribute("userdetailsinfo") != null){
				String loginName = ((TblUser)request1.getSession().getAttribute("userdetailsinfo")).getLoginName();
				
			

		    MultipartHttpServletRequest request = (MultipartHttpServletRequest) request1;

			MultipartFile fileToSave = request.getFile("documentcontent1");
			byte[] byteFile = fileToSave.getBytes();
			ByteArrayInputStream stream = new   ByteArrayInputStream(byteFile);
			String myString = IOUtils.toString(stream, "UTF-8");		
			String[] arr = myString.split("\n");		
			
			String poolName = request.getParameter("PoolName");
/*			String poolDesc = request.getParameter("PoolDesc");
*/
/*			poolserviceimpl.savePool(arr,poolName, poolDesc);
*/			poolserviceimpl.savePool(arr,poolName, loginName);

			ModelMap model = new ModelMap();
			ArrayList<TblPool> poolList = new ArrayList<>();
			
			for(TblPool tblPool : poolserviceimpl.getPools(loginName)) {
				if (poolList.size() == 0)
				{
					poolList.add(tblPool);
				}
				else {
					int flag = 1;
					for(TblPool tblPoolTemp : poolList){
						if(tblPoolTemp.getPoolName().equals(tblPool.getPoolName())){
							flag = 0;
							break;
						}
					}
					if(flag == 1){
						poolList.add(tblPool);
					}					
				}
				
			}
	        model.addAttribute("pools", poolList);
			
		
		return new ModelAndView("poolsuccess", model);
		
		}
			request1.getSession().setAttribute("userdetailsinfo", null);
			return new ModelAndView("redirect:/view/login");
			
		}
		// upload file to add questions to existing pool in list Questions page 
		@RequestMapping("/uploadFiletoPool/{poolID}/{poolName}")	
		public ModelAndView uploadFiletoPool(HttpServletRequest  request1, @PathVariable Integer poolID, @PathVariable String poolName) throws IOException {
			
			if(request1.getSession().getAttribute("userdetailsinfo") != null){
				String loginName = ((TblUser)request1.getSession().getAttribute("userdetailsinfo")).getLoginName();
				
			

		    MultipartHttpServletRequest request = (MultipartHttpServletRequest) request1;

			MultipartFile fileToSave = request.getFile("documentcontent1");
			byte[] byteFile = fileToSave.getBytes();
			ByteArrayInputStream stream = new   ByteArrayInputStream(byteFile);
			String myString = IOUtils.toString(stream, "UTF-8");		
			String[] arr = myString.split("\n");		
			
/*			String poolName = request.getParameter("PoolName");
*//*			String poolDesc = request.getParameter("PoolDesc");
*/
/*			poolserviceimpl.savePool(arr,poolName, poolDesc);
*/			poolserviceimpl.savetoExistingPool(arr,poolName, loginName);

			ModelMap model = new ModelMap();
			model.addAttribute("poolName", poolName);
			model.addAttribute("poolID", poolID);

					
		return new ModelAndView("dummyPage", model);
		
		}
			request1.getSession().setAttribute("userdetailsinfo", null);
			return new ModelAndView("redirect:/view/login");
			
		}
		
		@RequestMapping(method = RequestMethod.GET, value = "/getPools")
		public @ResponseBody List<TblPool> getPools(HttpServletRequest  request) {
				String loginName = ((TblUser)request.getSession().getAttribute("userdetailsinfo")).getLoginName();
				
		return poolserviceimpl.getPools(loginName);
		
		}
		
		
		@RequestMapping(method = RequestMethod.GET, value = "/listPools")	
		public ModelAndView listPools(HttpServletRequest  request) {	
			if(request.getSession().getAttribute("userdetailsinfo") != null){
				String loginName = ((TblUser)request.getSession().getAttribute("userdetailsinfo")).getLoginName();
				
			ModelMap model = new ModelMap();
			
			ArrayList<TblPool> poolList = new ArrayList<>();
			
			for(TblPool tblPool : poolserviceimpl.getPools(loginName)) {
				if (poolList.size() == 0)
				{
					poolList.add(tblPool);
				}
				else {
					int flag = 1;
					for(TblPool tblPoolTemp : poolList){
						if(tblPoolTemp.getPoolName().equals(tblPool.getPoolName())){
							flag = 0;
							break;
						}
					}
					if(flag == 1){
						poolList.add(tblPool);
					}					
				}
				
			}
	    model.addAttribute("pools", poolList);
				
		return new ModelAndView("listPools", model);
			}
			request.getSession().setAttribute("userdetailsinfo", null);
			return new ModelAndView("redirect:/view/login");
		
		}	
		
		@RequestMapping(method = RequestMethod.GET, value = "/removePool/{poolID}")	
		public @ResponseBody String removePool(@PathVariable Integer poolID) {	
			
					
			poolserviceimpl.removePool(poolID);
		
		return "Pool has been deleted successfully";
		
		}
		
		
	
		
		@RequestMapping(method = RequestMethod.POST, value = "/updatePool")
		public @ResponseBody String updatePool(@RequestBody String jsonstring) throws JsonProcessingException, IOException {
			
			poolserviceimpl.updatePool(jsonstring);
			
			return "Pool has been updated successfully.";
		}

		
		
		@RequestMapping(method = RequestMethod.POST, value = "/updateQuestionOptions/{poolID}")
		public @ResponseBody String updateQuestionOptions(HttpServletRequest  request,@PathVariable String poolID, @RequestBody String totalQuestionOptions) throws JsonProcessingException, IOException {
			
			String loginName = ((TblUser)request.getSession().getAttribute("userdetailsinfo")).getLoginName();
			poolserviceimpl.updateQuestionOptions(totalQuestionOptions, Integer.parseInt(poolID));			
			
		  	return "Questions have been updated successfully.";
		}
		
		
		
		@RequestMapping(method = RequestMethod.GET, value = "/showQuestions/{poolID}/{poolName}")	
		public ModelAndView showQuestions(@PathVariable Integer poolID, @PathVariable String poolName) {	
		
			
			List<TblPoolquestion> 	questionList = poolserviceimpl.getQuestions(poolID);
 
			List<QuestionChoicesWrapperForUpdation> 	quesAnsForUpd = new ArrayList<QuestionChoicesWrapperForUpdation>();

			List<TblPoolquestionoption> choiceList1 = new ArrayList<TblPoolquestionoption>();
							            
				
				for (int i =0; i< questionList.size();i++) {
							
					choiceList1 = poolserviceimpl.getChoices(questionList.get(i).getPoolQuestionID());
					List<String> choiceList = new ArrayList<String>();
					
					int correctChoice =0, count = 0;
					
					for(int j =0; j< choiceList1.size(); j++){
						choiceList.add(choiceList1.get(j).getChoice());
						count++;
						if(choiceList1.get(j).getFractionCorrect() == 1){							
							correctChoice = count;
						}
						
					}
					
					QuestionChoicesWrapperForUpdation qcwfu = new QuestionChoicesWrapperForUpdation(questionList.get(i).getPoolQuestionID(),i+1, questionList.get(i).getQuestion(), choiceList, correctChoice);
					quesAnsForUpd.add(qcwfu);
				}
				
				TblPool tblPool = poolRepository.findTblPooltBypoolID(poolID);
				ModelMap model = new ModelMap();
				model.addAttribute("poolID",poolID);
				model.addAttribute("poolName",tblPool.getPoolName());
				model.addAttribute("questionOptions", quesAnsForUpd);
				
						
		return new ModelAndView("listQuestions",model);
		
	}
		
		
		@RequestMapping(method = RequestMethod.GET, value = "/removeQuestions/{poolQuestionID}")	
		public @ResponseBody String removeQuestions(@PathVariable Integer poolQuestionID) {	
			
					
			poolserviceimpl.removeQuestions(poolQuestionID);
		
		return "Questions have been deleted successfully";
		
		}
		
		@RequestMapping(method = RequestMethod.GET, value = "/addQuestion/{poolID}/{poolName}")	
		public ModelAndView addQuestion(@PathVariable Integer poolID, @PathVariable String poolName) {	
			
			
			ModelMap model = new ModelMap();
			model.addAttribute("poolID",poolID);
			model.addAttribute("pname", poolName);
			
			
		return new ModelAndView("addQuestion", model);
		}
		
		
		
					
		
		@RequestMapping(method = RequestMethod.POST, value = "/savenewQuestionOptions/{poolID}")
		public @ResponseBody String savenewQuestionOptions(@PathVariable Integer poolID, @RequestBody String newQuestionOptions) throws JsonProcessingException, IOException {
			
			
			poolserviceimpl.savenewQuestionOptions(newQuestionOptions, poolID);			
			
		  	return "Questions have been added successfully.";
		}
		
		
		
		@RequestMapping(method = RequestMethod.GET, value = "/showGrades")
		public ModelAndView showGrades() {
		
			ModelMap model = new ModelMap();
			
			List<TblStudentquiz> studentQuizList = poolserviceimpl.findAllTblStudentquiz();
			List<String> studentNames = new ArrayList<>();
/*			List<String> quizNames = new ArrayList<>();
*/
			for (TblStudentquiz t:studentQuizList ) {
				TblUser tbluser = userServiceImpl.findbyuserID(t.getTblStudent().getStudentID()); 
				studentNames.add(tbluser.getName());
/*				quizNames.add(t.getTblQuiz().getQuizName());
*/                
			}
/*	        model.addAttribute("studentQuizzes", studentQuizList);
*/	        model.addAttribute("studentNames", studentNames);
/*	        model.addAttribute("quizNames", quizNames);
*/			
			
	        model.addAttribute("studentQuizzes", studentQuizList);
	
			 
			return new ModelAndView("quizGrades",model);
		}
		
		
		@RequestMapping(method = RequestMethod.POST, value = "/setAccess")
		public @ResponseBody String setQuizAccess(@RequestBody String json) throws JsonProcessingException, IOException {
			
			poolserviceimpl.saveStudentQuizAcess(json);			
			
		  	return "Students have been given access to assignments successfully";
		}
		
		@RequestMapping(method = RequestMethod.GET, value = "/checkPoolName/{poolName}")
		public @ResponseBody boolean checkPoolName(HttpServletRequest  request, @PathVariable String poolName)  {
			String loginName = ((TblUser)request.getSession().getAttribute("userdetailsinfo")).getLoginName();
			return poolserviceimpl.checkPoolName(poolName, loginName);
		}
		
		@RequestMapping(method = RequestMethod.GET, value = "/checkPoolNameAndSave/{poolID}/{poolName}")
		public @ResponseBody boolean checkPoolNameAndSave(HttpServletRequest  request, @PathVariable String poolID, @PathVariable String poolName)  {
				
			String loginName = ((TblUser)request.getSession().getAttribute("userdetailsinfo")).getLoginName();	
			return poolserviceimpl.checkPoolNameAndSave(Integer.parseInt(poolID), poolName, loginName);
		}
		
		
		@RequestMapping(method = RequestMethod.GET, value = "/exportFile/{poolID}/{poolName}")
		public @ResponseBody String exportFile(HttpServletRequest  request, @PathVariable Integer poolID, @PathVariable String poolName) throws FileNotFoundException  {
				
			String fileName = poolName;
			PrintWriter myWriter = new PrintWriter(new File(fileName + ".txt"));
			
			List<TblPoolquestion> qlist = poolserviceimpl.getQuestions(poolID);	
			
						for (int i = 0; i < qlist.size(); i++) { 
							myWriter.println(qlist.get(i).getQuestion());
							System.out.println(qlist.get(i).getQuestion());
							List<TblPoolquestionoption> choicesList =	poolserviceimpl.getChoices(qlist.get(i).getPoolQuestionID());
							
							for (TblPoolquestionoption choices : choicesList )
							{
								if(choices.getFractionCorrect() == 1){
									myWriter.println(choices.getChoice() + "\t" + "CORRECT ANSWER");
									System.out.println(choices.getChoice());
								}else{
									myWriter.println(choices.getChoice());
									System.out.println(choices.getChoice());
								}
							}
						}
			myWriter.close();
			return fileName;
		}
		
		public class QuestionChoicesWrapperForUpdation {
			
			int poolQuestionID;
			int questionNo;
			String question;
			List<String> choices;
			int correctChoice;
			

			public QuestionChoicesWrapperForUpdation() {
				super();
			}


			public String getQuestion() {
				return question;
			}


			public void setQuestion(String question) {
				this.question = question;
			}


			public List<String> getChoices() {
				return choices;
			}


			public void setChoices(List<String> choices) {
				this.choices = choices;
			}


			public int getCorrectChoice() {
				return correctChoice;
			}


			public void setCorrectChoice(int correctChoice) {
				this.correctChoice = correctChoice;
			}
			
			

			public int getQuestionNo() {
				return questionNo;
			}


			public void setQuestionNo(int questionNo) {
				this.questionNo = questionNo;
			}


			public int getPoolQuestionID() {
				return poolQuestionID;
			}


			public void setPoolQuestionID(int poolQuestionID) {
				this.poolQuestionID = poolQuestionID;
			}


			public QuestionChoicesWrapperForUpdation(int poolQuestionID, int questionNo, String question, List<String> choices, int correctChoice) {
				super();
				this.poolQuestionID=poolQuestionID;
				this.questionNo = questionNo;
				this.question = question;
				this.choices = choices;
				this.correctChoice = correctChoice;
			}
			
			
		}
		
		
		

}
